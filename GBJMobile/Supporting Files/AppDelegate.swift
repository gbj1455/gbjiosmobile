//
//  AppDelegate.swift
//  GBJMobile
//
//  Created by Abdullah Soylemez on 7.10.2021.
//

import Firebase
import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = SplashViewController.init()
        window?.makeKeyAndVisible()
        
        FirebaseApp.configure()
        
        return true
    }
}
