//  
//  SplashViewModelProtocol.swift
//  GBJMobile
//
//  Created by Abdullah Soylemez on 11.10.2021.
//  Copyright © 2021 Grand Bazaar Jewelers. All rights reserved.
//

import Foundation

protocol SplashViewModelProtocol: AnyObject {
    var delegate: SplashViewModelDelegate? { get set }
    func load()
}

protocol SplashViewModelDelegate: AnyObject {
    func handle(output: SplashViewModelOutput)
}

enum SplashViewModelOutput {
    case fetched
}
