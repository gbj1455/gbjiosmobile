//  
//  SplashViewController.swift
//  GBJMobile
//
//  Created by Abdullah Soylemez on 11.10.2021.
//  Copyright © 2021 Grand Bazaar Jewelers. All rights reserved.
//

import UIKit

final class SplashViewController: UIViewController {
    var viewModel: SplashViewModelProtocol? {
        didSet {
            viewModel?.delegate = self
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel?.load()
    }
}

// MARK: - SplashViewModelDelegate
extension SplashViewController: SplashViewModelDelegate {
    func handle(output: SplashViewModelOutput) {
        switch output {
        case .fetched:
            return
        }
    }
}
