//  
//  SplashViewModel.swift
//  GBJMobile
//
//  Created by Abdullah Soylemez on 11.10.2021.
//  Copyright © 2021 Grand Bazaar Jewelers. All rights reserved.
//

import Foundation

final class SplashViewModel: SplashViewModelProtocol {
    weak var delegate: SplashViewModelDelegate?

    // MARK: - Public functions
    func load() {
        // load
    }
}
