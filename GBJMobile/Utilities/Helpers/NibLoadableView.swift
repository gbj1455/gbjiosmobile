//
//  NibLoadableView.swift
//  GBJMobile
//
//  Created by Abdullah Soylemez on 7.10.2021.
//  Copyright © 2021 Grand Bazaar Jewelers. All rights reserved.
//

import UIKit

class NibLoadableView: UIView {
    var contentView: UIView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
        setup()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configureView()
        setup()
    }

    func configureView() {
        guard let view = loadViewFromNib(nibName: className) else { return }
        xibSetup(contentView: view)
        contentView = view
    }

    func setup() {}
}
