//
//  NibLoadable.swift
//  GBJMobile
//
//  Created by Abdullah Soylemez on 7.10.2021.
//  Copyright © 2021 Grand Bazaar Jewelers. All rights reserved.
//

import UIKit

protocol NibLoadable: AnyObject {
    static var nib: UINib { get }
}

extension NibLoadable {
    static var nib: UINib {
        UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
}
