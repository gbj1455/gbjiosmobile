//
//  NSObject+Extensions.swift
//  GBJMobile
//
//  Created by Abdullah Soylemez on 7.10.2021.
//  Copyright © 2021 Grand Bazaar Jewelers. All rights reserved.
//

import Foundation

extension NSObject {
    var className: String {
        NSStringFromClass(self.classForCoder).components(separatedBy: ".").last ?? ""
    }
}
