//
//  UIView+Extensions.swift
//  GBJMobile
//
//  Created by Abdullah Soylemez on 7.10.2021.
//  Copyright © 2021 Grand Bazaar Jewelers. All rights reserved.
//

import UIKit

extension UIView {
    @discardableResult
    func loadViewFromNib(nibName: String, in bundle: Bundle? = nil) -> UIView? {
        let nib = UINib(nibName: nibName, bundle: bundle ?? Bundle(for: type(of: self)))
        let view = nib.instantiate(withOwner: self, options: nil).first as? UIView
        return view
    }
    
    func xibSetup(contentView: UIView) {
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(contentView)
    }
}
