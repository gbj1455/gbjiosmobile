//
//  UIColor+GBJ.swift
//  GBJMobile
//
//  Created by Abdullah Soylemez on 7.10.2021.
//  Copyright © 2021 Grand Bazaar Jewelers. All rights reserved.
//

import UIKit

extension UIColor {
    class var white: UIColor {
        return UIColor(white: 1.0, alpha: 1.0)
    }

    class var brownishGrey: UIColor {
        return UIColor(white: 110.0 / 255.0, alpha: 1.0)
    }

    class var warmGrey: UIColor {
        return UIColor(white: 112.0 / 255.0, alpha: 1.0)
    }

    class var wheat: UIColor {
        return UIColor(red: 1.0, green: 223.0 / 255.0, blue: 128.0 / 255.0, alpha: 1.0)
    }

    class var paleOrange: UIColor {
        return UIColor(red: 1.0, green: 186.0 / 255.0, blue: 89.0 / 255.0, alpha: 1.0)
    }

    class var darkBeige: UIColor {
        return UIColor(red: 179.0 / 255.0, green: 152.0 / 255.0, blue: 96.0 / 255.0, alpha: 1.0)
    }

    class var black: UIColor {
        return UIColor(white: 0.0, alpha: 1.0)
    }

    class var blackTwo: UIColor {
        return UIColor(white: 40.0 / 255.0, alpha: 1.0)
    }
}
