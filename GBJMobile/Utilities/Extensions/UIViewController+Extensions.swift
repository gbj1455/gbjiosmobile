//
//  UIViewController+Extensions.swift
//  GBJMobile
//
//  Created by Abdullah Soylemez on 7.10.2021.
//  Copyright © 2021 Grand Bazaar Jewelers. All rights reserved.
//

import UIKit

extension UIViewController {
    class func instance<T: UIViewController>(
        from name: String = String("\(T.self)"),
        bundle: Bundle? = Bundle(for: T.self)
    ) -> T {
        if bundle?.path(forResource: name, ofType: "nib") != nil {
            return T(nibName: name, bundle: bundle)
        }
        
        return T()
    }
}
